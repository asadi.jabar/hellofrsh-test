<?php

namespace App\HelloFresh\Controller;

/**
 * base controller can be used for extending the functionality of other controllers
 *
 * Class BaseController
 *
 * @package App\HelloFresh\Controller
 */
class BaseController
{

}