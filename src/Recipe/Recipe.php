<?php

namespace App\HelloFresh\Recipe;


/**
 * Class Recipe
 *
 * @package App\HelloFresh\Recipe
 *
 * @method static \App\HelloFresh\Recipe\Builder create (array $data, \Closure $callback)
 * @method static \App\HelloFresh\Recipe\Builder delete (array $id, \Closure $callback = null)
 * @method static \App\HelloFresh\Recipe\Builder update (array $data, \Closure $callback)
 * @method static \App\HelloFresh\Recipe\Builder get (array $id)
 * @method static \App\HelloFresh\Recipe\Builder rate (array $data, \Closure $callback)
 * @method static \App\HelloFresh\Recipe\Builder search (array $data, \Closure $callback)
 *
 * @see \App\HelloFresh\Recipe\Builder
 */
class Recipe extends AbstractRecipe
{

    /**
     * @inheritdoc
     *
     * @return Builder|mixed
     */
    public static function getRecipeAccessor()
    {
        return new Builder();
    }
}